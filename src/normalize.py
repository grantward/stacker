"""Provides standardized naming for objects."""

import re


def normalize_name(name):
    """Normalize name, ie. translate whitespace and lower case."""
    normalized = name.lower()
    normalized = normalized.strip()
    normalized = re.sub(r'\s+', '-', normalized)
    return normalized


def pretty_name(name):
    """Prettify a name, essentially de-normalizing it."""
    name = name.strip()
    words = name.split('-')
    words = [word[0].upper() + word[1:] for word in words if word]
    return ' '.join(words)

if __name__ == '__main__':
    pass
