#!/usr/bin/env python3
"""Retrieve current market prices."""

import requests

from . import html_reader, normalize


# Hardcoded business insider commodities page URL
BI_URL = 'https://markets.businessinsider.com/commodities'
URL_KEY = {
    'business-insider': BI_URL,
}
PARSER_KEY = {
    'business-insider': html_reader.BusinessInsiderParser,
}


def default_name():
    """Get a reasonable default name.
    TODO: implement some sort of algorithmic pick.
    """
    return 'business-insider'


def get_page(name):
    """Get the page associated with name, useful for building the search string."""
    name = normalize.normalize_name(name)
    url = URL_KEY.get(name)
    if not url:
        _invalid_name(name)
    page = requests.get(url, timeout=10)
    return page


def parse_page(name):
    """Parse the page data associated with name for prices. Returns a dictionary of price data."""
    name = normalize.normalize_name(name)
    page = get_page(name)
    parser_class = PARSER_KEY.get(name)
    if not parser_class:
        raise ValueError(f'No parser available for: {name}')
    parser = parser_class()
    parser.feed(page.text)
    return parser.prices


def _invalid_name(name):
    """Raise an error because the provided name is not recognized."""
    choices = ', '.join(URL_KEY.keys())
    raise ValueError(f'Name: {name} is not recognized. Choose from: {choices}')


if __name__ == '__main__':
    pass
