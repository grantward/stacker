"""Provides HTML processing to find price data."""

from decimal import Decimal
from html.parser import HTMLParser
import re

from src import common


class BusinessInsiderParser(HTMLParser):
    """Parses the Business Insider commodities page for price data."""

    def __init__(self, *args, **kwargs):
        """Initialize a new parser, making sure to pass data to parent."""
        super().__init__(*args, **kwargs)
        self.current_title = None
        self.current_data = None
        self.read_price = False
        self.prices = {}

    def handle_starttag(self, tag, attrs):
        """Opening tag. Find td."""
        if tag == 'a':
            attrs = dict(attrs)
            title = attrs.get('title', '').lower()
            if title in common.PRECIOUS_METALS:
                # We potentially found a price row
                self.current_title = title
        elif tag == 'span':
            if self.current_title and self.current_data:
                # We found a price cell
                self.read_price = True

    def handle_data(self, data):
        """Data inside tag."""
        if self.current_title:
            # We potentially found a price row
            data = data.lower()
            if self.read_price:
                # Check if we found a valid price
                if re.fullmatch(r'[0-9,]+\.\d+', data):
                    # Found the price text. Save it.
                    self.prices[self.current_title] = Decimal(data.replace(',', ''))
                self.current_title = None
                self.current_data = None
                self.read_price = False
            elif data == self.current_title:
                # Name in tag matches title, we found a price row
                self.current_data = data


if __name__ == '__main__':
    pass
