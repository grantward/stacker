#!/usr/bin/env python3
"""Main entrypoint to the stacker program."""

import argparse
from decimal import Decimal
import sys

from . import common, mode, model


def main(argv):
    """Entrypoint function."""
    parser, args = _get_args(argv)

    modes = {
        'add': mode.add_to_stack,
        'remove': mode.remove_from_stack,
        'show': mode.show_stack,
        'database': mode.database_mode,
        None: lambda _: parser.print_help(),
    }
    # Initialize the commodities data
    model.init_commodities()
    # Without a subcommnd specified, 'show' mode is entered
    try:
        modes[args.mode](args)
        return 0
    except ValueError as e:
        print(e, file=sys.stderr)
        return 1


def _get_args(argv):
    """Retrieve argument namespace from command line arguments."""
    parser = argparse.ArgumentParser(
        prog='Stacker',
        description='Tracks the quantity and market value of precious metals stacks',
    )
    parser.add_argument(
        '-j',
        '--json-stack-file',
        default='stack.json',
        help='JSON file to record the stack in',
    )
    subparsers = parser.add_subparsers(dest='mode')

    # Show mode, display stack size and value
    show = subparsers.add_parser('show', help='Show the size and value of your stack')
    show.add_argument(
        '-m',
        '--metals',
        type=_to_set,
        default=set(common.PRECIOUS_METALS),
        help='Limit to the specified metals. Comma-separated list, ie. gold,silver'
    )
    show.add_argument(
        '-f',
        '--full-contents',
        action='store_true',
        help='Show the full contents of the stack',
    )

    # Database mode, view item info
    subparsers.add_parser('database', help='View available items in the database')

    # Add mode, add to the stack
    add = subparsers.add_parser('add', help='Add to your stack')
    add.add_argument(
        '-n',
        '--name',
        required=True,
        help='The name of the item to add. Ie. "silver eagle". "-" can be used in place of spaces.'
    )
    add.add_argument(
        '-q',
        '--quantity',
        required=True,
        type=int,
        help='The amount of this item you are adding to the stack.'
    )
    add.add_argument(
        '-p',
        '--price-each',
        type=Decimal,
        default=Decimal(0),
        help='The price of each individual item you are adding'
    )
    # Remove mode, remove from the stack
    remove = subparsers.add_parser('remove', help='Remove items from your stack')
    remove.add_argument(
        '-n',
        '--name',
        required=True,
        help=(
            'The name of the item to remove. Ie. "silver eagle". '
            '"-" can be used in place of spaces.'
        )
    )
    remove.add_argument(
        '-q',
        '--quantity',
        required=True,
        type=int,
        help='The amount of this item you are removing from the stack.'
    )

    # Retrieve and process args
    return parser, parser.parse_args(argv)


def _to_list(input_str):
    """Build a list of strings from the comma-separated input provided."""
    return input_str.split(',')


def _to_set(input_str):
    """Build a set of strings from the comma-separated input provided."""
    return set(_to_list(input_str))


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
