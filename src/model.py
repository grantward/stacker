"""Provides commodity item modelling. Reads in a description from JSON and builds
a corresponding object representing that commodity.
"""

from decimal import Decimal
import glob
import json
from os import path

from . import common, normalize

DEFAULT_DATA_DIR = './data/commodity'
# List of commodity objects from JSON
COMMODITIES = None
# Index of names mapped to commodity objects
COMMODITY_INDEX = None

# Unit conversions
GRAMS_PER = {
    'ozt': Decimal('31.1035'),
    'g': Decimal('1'),
    'lb': Decimal('453.592'),
}

def init_commodities(data_dir=None):
    """Build the global list of commodities from JSON."""
    global COMMODITIES
    global COMMODITY_INDEX

    data_dir = path.abspath(data_dir if data_dir else DEFAULT_DATA_DIR)
    data_glob_path = path.join(data_dir, '*')
    data_glob = glob.glob(data_glob_path)
    COMMODITIES = Commodity.list_from_glob(data_glob)
    # Build the index
    COMMODITY_INDEX = {}
    for commodity in COMMODITIES:
        for name in commodity.names:
            COMMODITY_INDEX[name] = commodity


class Commodity:
    """Represents a specific type of commodity, ie. 1oz silver round.
    All values are in g unless otherwise specified.
    """
    # pylint: disable=too-many-arguments
    def __init__(self, names, composition=None, unit=None, percent=None, mass=None, inherit=None):
        """Build a new commodity object.
        Names are unique string identifiers of this commodity.
        Composition provides exact measurements of component elements.
        Inherit takes composition data from another commodity.
        """
        self.names = names
        self._composition = composition
        self._unit = unit
        if self._unit and not self._composition:
            raise ValueError('If "unit" is used, "composition" is required.')
        self._percent = percent
        self._mass = mass
        if bool(self._percent) != bool(self._mass):
            raise ValueError('If "percent" is used, "mass" is required, and vice versa.')
        if self._composition and self._percent:
            raise ValueError(
                'Cannot specify via both "composition" and "percent", '
                'use one or the other.'
            )
        self.inherit = inherit
        if bool(self.inherit) == (self._composition or self._percent):
            raise ValueError(
                'Cannot specify "inherit" but also "composition" or "percent", '
                'use one or the other.'
            )

    def quantity(self, metal, unit):
        """Get the quantity of a given metal in 1 unit of this commodity, outputted in the
        given unit.
        """
        result = Decimal(0)
        from_grams = GRAMS_PER[unit]
        to_grams = GRAMS_PER[self.unit]
        result = self.composition.get(metal, Decimal(0)) * to_grams / from_grams
        return result

    def get_from_self_or_parent(self, attr, default=None):
        """Get attribute from this object, or from parent if not present
        on this object.
        """
        found = getattr(self, attr)
        if not found:
            if self.inherit:
                # Remove underscore so we continue to recurse
                fixed_attr = attr.strip('_')
                found = getattr(COMMODITY_INDEX.get(self.inherit), fixed_attr)
        return found if found else default if default else found

    @property
    def name(self):
        """Get the primary/first name for this item."""
        return self.names[0]

    @property
    def composition(self):
        """Get composition from this object if available, if not look for parent."""
        built = self.get_from_self_or_parent('_composition')
        if not built:
            # Composition not specified, build from mass/percentages
            percent = self.percent
            mass = self.mass
            specified_percent = sum(percent.values())
            slag_percent = Decimal(1) - specified_percent
            # Calculate mass per component in grams
            built = {
                metal: percentage * mass
                for metal, percentage in percent.items()
            }
            # Calculate and add slag
            built['slag'] = slag_percent * self.mass
            # Convert from grams
            built = {
                metal: quantity / GRAMS_PER['ozt']
                for metal, quantity in built.items()
            }
        return built

    @property
    def percent(self):
        """Get percent (of components) from this object if available,
        if not look for parent.
        """
        return self.get_from_self_or_parent('_percent')

    @property
    def unit(self):
        """Get unit of composition for this object, or a default."""
        return self.get_from_self_or_parent('_unit', 'ozt')

    @property
    def mass(self):
        """Get mass (in g) for this object, or from parent if inheriting."""
        built = self.get_from_self_or_parent('_mass')
        if not built:
            # Mass not specified, build from composition
            multiplier = GRAMS_PER[self.unit]
            built = sum((each * multiplier for each in self.composition.values()))
        return built

    @classmethod
    def from_dict(cls, data):
        """Build a commodity object from a data dictionary (presumably from JSON)."""
        name = data.get('name')
        names = data.get('names', [])
        composition = data.get('composition')
        percent = data.get('percent')
        mass = data.get('mass')
        inherit = data.get('inherit')
        unit = data.get('unit')

        # Combine name and names fields
        if not (name or names):
            raise ValueError(f'Commodity: {data} name/names field is required.')
        names = [name] + names if name else names
        # Normalize string names coming from JSON, just in case
        names = [normalize.normalize_name(name) for name in names]

        # Translate JSON-compatible strings to Decimal values
        composition = {
            element: Decimal(grams) for element, grams in composition.items()
        } if composition else None
        percent = {
            element: Decimal(grams) for element, grams in percent.items()
        } if percent else None
        mass = Decimal(mass) if mass else None

        # Build object
        try:
            return cls(
                names=names,
                composition=composition,
                unit=unit,
                percent=percent,
                mass=mass,
                inherit=inherit
            )
        except ValueError as err:
            raise ValueError(f'Commodity: {data} is not valid.') from err

    @classmethod
    def list_from_json(cls, json_f):
        """Build a list of commodity objects from a file object containing JSON."""
        data_dicts = json.load(json_f)
        try:
            return [cls.from_dict(data) for data in data_dicts]
        except ValueError as err:
            raise ValueError(f'Failed to process file: {json_f.name}') from err

    @classmethod
    def list_from_glob(cls, found_glob):
        """Build a list of commodity objects from a glob of JSON filenames."""
        built = []
        for filename in found_glob:
            with open(filename, 'r', encoding='utf-8') as json_f:
                built.extend(cls.list_from_json(json_f))
        return built


class Inventory:
    """A collection of items to be summarized."""
    def __init__(self, stack, price_data):
        """Build an inventory from a stack (from JSON) and current price data."""
        self.stack = stack
        self.price_data = price_data
        self.contents = []

        for name, item in self.stack.items():
            item_data = COMMODITY_INDEX[name]
            # Quantity of each metal for all the examples of this item
            quantities = {
                metal: item_data.quantity(metal, unit='ozt') * item['quantity']
                for metal in common.PRECIOUS_METALS
            }
            # Melt value per metal for all the examples of this item
            melt_values = {
                metal: metal_quantity * price_data.get(metal, Decimal(0))
                for metal, metal_quantity in quantities.items()
            }
            # Total melt value of all metals
            total_melt = sum(melt_values.values())
            self.contents.append(
                {
                    'name': item_data.name,
                    'quantity': item['quantity'],
                    'quantities': quantities,
                    'melt_values': melt_values,
                    'total_melt': total_melt,
                    'total_mass': item_data.mass * item['quantity'],
                }
            )

    def quantity(self, metal):
        """Get the total quantity of a given metal from this stack."""
        return sum(
            (
                content['quantities'].get(metal, Decimal(0))
                for content in self.contents
            )
        )

    def total_mass(self):
        """Get the total mass of all items in this stack."""
        return sum((content['total_mass'] for content in self.contents))

    def melt_value(self, metal):
        """Get the total melt value of a given metal from this stack."""
        return self.price_data.get(metal, Decimal(0)) * self.quantity(metal)

    def total_melt_value(self):
        """Get the total melt value of this stack."""
        return sum((self.melt_value(metal) for metal in common.PRECIOUS_METALS))


if __name__ == '__main__':
    pass
