"""Implements program modes for the stacker program."""

import copy
from decimal import Decimal
import json
import time

from tabulate import tabulate

from . import model, normalize, price


# Only update price if 5 minutes has passed
PRICE_UPDATE_INTERVAL = 5 * 60
# Round decimals to this level
DEFAULT_ROUND = 2
# Default unit to display weight / mass of stack
MASS_UNIT = 'lb'


def add_to_stack(args):
    """Mode allowing user to add material to the stack."""
    # Process cmdline args
    json_stack_file = args.json_stack_file
    name = args.name
    normalized_name = normalize.normalize_name(name)
    if normalized_name not in model.COMMODITY_INDEX:
        # Provided item not found, cannot be added to stack
        raise ValueError(f'Cannot add [{name}] to stack. This item does not exist in the database.')
    quantity = args.quantity
    price_each = args.price_each

    # Get current stack
    stack_blob = read_current_stack(json_stack_file)
    stack = stack_blob['stack'] = stack_blob.get('stack', {})

    # Compute new quantity and average prices
    existing = stack[normalized_name] = stack.get(normalized_name, {})
    existing_quantity = existing.get('quantity', 0)
    existing_avg_price = existing.get('avg_price', 0)
    new_quantity = existing_quantity + quantity
    new_avg_price = (
        (existing_avg_price * existing_quantity + price_each * quantity) /
        new_quantity
    )

    # Build record
    existing['quantity'] = new_quantity
    existing['avg_price'] = new_avg_price

    # Output new stack to file
    write_current_stack(json_stack_file, stack_blob)


def remove_from_stack(args):
    """Mode allowing removal of items from stack."""
    json_stack_file = args.json_stack_file
    json_stack_file = args.json_stack_file
    name = args.name
    normalized_name = normalize.normalize_name(name)
    if normalized_name not in model.COMMODITY_INDEX:
        # Provided item not found, cannot be added to stack
        raise ValueError(f'Cannot remove [{name}] from stack. '
                         'This item does not exist in the database.')
    quantity = args.quantity

    # Get current stack
    stack_blob = read_current_stack(json_stack_file)
    stack = stack_blob['stack'] = stack_blob.get('stack', {})

    # Compute new quantity and average prices
    existing = stack[normalized_name] = stack.get(normalized_name, {})
    existing_quantity = existing.get('quantity', 0)
    new_quantity = existing_quantity - quantity
    if new_quantity < 0:
        raise ValueError(f'Cannot remove {quantity} of {normalized_name} '
                         'from present {existing_quantity}')

    # Build record
    existing['quantity'] = new_quantity

    # Output new stack to file
    write_current_stack(json_stack_file, stack_blob)


def show_stack(args):
    """Mode allowing user to display the size and value of the stack."""
    # Process cmdline args
    json_stack_file = args.json_stack_file
    metals = args.metals
    full_contents = args.full_contents

    # Read current stack from file
    stack_blob = read_current_stack(json_stack_file)
    stack = stack_blob.get('stack', {})

    # Get current price data
    price_last_updated = stack_blob.get('price_updated', 0)
    price_updated = time.time()
    if price_updated - price_last_updated > PRICE_UPDATE_INTERVAL:
        price_source = price.default_name()
        current_prices = price.parse_page(price_source)
        # Store price data in JSON blob and write to file
        stack_blob['price_updated'] = price_updated
        stack_blob['price_source'] = price_source
        stack_blob['prices'] = current_prices
        write_current_stack(json_stack_file, stack_blob)
    else:
        price_source = stack_blob.get('price_source')
        current_prices = stack_blob.get('prices', {})

    # Build inventory (quantity)
    inventory = model.Inventory(stack, current_prices)
    # Total mass/weight of stack
    unit_mass = inventory.total_mass() / model.GRAMS_PER[MASS_UNIT]

    # Build tables
    # Table showing current prices for each metal
    price_table = sorted(
        (
            (normalize.pretty_name(metal), current_price)
            for metal, current_price in current_prices.items()
        )
    )
    # Table showing current quantity and value of each metal in stack
    stack_table = [
        (
            normalize.pretty_name(metal),
            f'{inventory.quantity(metal):.{DEFAULT_ROUND}f}',
            f'${inventory.melt_value(metal):.{DEFAULT_ROUND}f}',
        )
        for metal in sorted(metals)
        if inventory.quantity(metal)
    ]

    # Print off data to user
    # Price info
    print(f'CURRENT METAL PRICES (via {normalize.pretty_name(price_source)}):')
    # TODO: other currencies/units
    print(tabulate(price_table, headers=('Metal', 'Price (USD)')))
    print()

    print('CURRENT STACK:')
    print(f'Total Value: ${inventory.total_melt_value():.{DEFAULT_ROUND}f}')
    print(f'Weight: {unit_mass:.{DEFAULT_ROUND}f} {MASS_UNIT}')
    # TODO: other currencies/units
    print(tabulate(stack_table, headers=('Metal', 'Quantity (ozt)', 'Melt Value (USD)')))
    print()

    if full_contents:
        inventory_table = sorted(
            (
                (normalize.pretty_name(each['name']), each['quantity'], each['total_melt'])
                for each in inventory.contents
            ),
            key=lambda row: row[2],
            reverse=True
        )
        print(tabulate(inventory_table, headers=('Item', 'Quantity', 'Melt Value (USD)')))


def database_mode(_):
    """Dump information on the database (ie. available item models)."""
    commodity_table = sorted(
        (
            (name, commodity.composition)
            for name, commodity in model.COMMODITY_INDEX.items()
        )
    )
    print(tabulate(commodity_table, headers=('Item', 'Composition (ozt)')))


def read_current_stack(filename):
    """Attempt to read the current stack from JSON file."""
    stack = {}
    try:
        with open(filename, 'r', encoding='utf-8') as stack_f:
            stack = json.load(stack_f)
    except FileNotFoundError:
        # Stack file does not exist, start with a blank stack
        pass

    # Translate str to Decimals
    prices = stack.get('prices', {})
    for commodity, value in prices.items():
        prices[commodity] = Decimal(value)

    for record in stack.get('stack', {}).values():
        record['avg_price'] = Decimal(record.get('avg_price', '0.00'))

    return stack


def write_current_stack(filename, stack_blob):
    """Attempt to write the current stack to JSON file."""
    # Avoid making changes to live data
    stack_blob = copy.deepcopy(stack_blob)
    # Translate Decimals to str
    prices = stack_blob.get('prices', {})
    for commodity, value in prices.items():
        prices[commodity] = str(value)

    for record in stack_blob.get('stack', {}).values():
        record['avg_price'] = str(record.get('avg_price', '0.00'))

    # Write blob
    with open(filename, 'w', encoding='utf-8') as stack_f:
        json.dump(stack_blob, stack_f)


if __name__ == '__main__':
    pass
